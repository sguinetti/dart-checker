package com.DartChecker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class statistik extends AppCompatActivity {

    private final DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    private final DecimalFormat formater = new DecimalFormat("###.##", symbols);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_statistik);

        ArrayAdapter<MainActivity.spieler> arrayAdapter = new ArrayAdapter<>(
                statistik.this, R.layout.spinner_item_head, MainActivity.allespieler);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        final Spinner spielera = findViewById(R.id.spielera);
        final Spinner spielerb = findViewById(R.id.spielerb);
        final TextView pf1 = findViewById(R.id.pfeile1);
        final TextView pf2 = findViewById(R.id.pfeile2);
        final TextView ds1 = findViewById(R.id.durchschnitt1);
        final TextView ds2 = findViewById(R.id.durchschnitt2);
        final TextView spg1 = findViewById(R.id.spielegesamt1);
        final TextView spg2 = findViewById(R.id.spielegesamt2);
        final TextView spe1 = findViewById(R.id.einzel1);
        final TextView spe2 = findViewById(R.id.einzel2);
        final TextView siege1 = findViewById(R.id.siege1);
        final TextView siege2 = findViewById(R.id.siege2);
        final TextView u1801 = findViewById(R.id.u1801);
        final TextView u1802 = findViewById(R.id.u1802);
        final TextView u1401 = findViewById(R.id.u1401);
        final TextView u1402 = findViewById(R.id.u1402);
        final TextView u1001 = findViewById(R.id.u1001);
        final TextView u1002 = findViewById(R.id.u1002);
        final TextView u601 = findViewById(R.id.u601);
        final TextView u602 = findViewById(R.id.u602);
        final TextView besthit1 = findViewById(R.id.besterwurf1);
        final TextView besthit2 = findViewById(R.id.besterwurf2);
        final Button playera = findViewById(R.id.buttonspielera);
        final Button playerb = findViewById(R.id.buttonspielerb);
        final Button alle = findViewById(R.id.alle);
        final TextView setlegs1 = findViewById(R.id.setlegs1);
        final TextView setlegs2 = findViewById(R.id.setlegs2);
        final TextView check1 = findViewById(R.id.checkout1);
        final TextView check2 = findViewById(R.id.checkout2);
        final TextView setlegs1lost = findViewById(R.id.setlegs1lost);
        final TextView setlegs2lost = findViewById(R.id.setlegs2lost);
        final TextView quote1 = findViewById(R.id.quote1);
        final TextView quote2 = findViewById(R.id.quote2);

        spielera.setAdapter(arrayAdapter);
        spielerb.setAdapter(arrayAdapter);
        if (arrayAdapter.getCount() > 0) spielera.setSelection(0);
        if (arrayAdapter.getCount() > 1) spielerb.setSelection(1);

        playera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.willstStatistik) + spielera.getSelectedItem().toString() + getResources().getString(R.string.wirklichl));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jal),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spielerdaten komplett löschen
                                int index = spielera.getSelectedItemPosition();
                                MainActivity.spieler dummyspieler = MainActivity.allespieler.get(index);
                                dummyspieler.durchschnitt = 0;
                                dummyspieler.geworfenePfeile = 0;
                                dummyspieler.AnzahlSpiele = 0;
                                dummyspieler.anzahlSiege = 0;
                                dummyspieler.AnzahlEinzelspiele = 0;
                                dummyspieler.anzahl180 = 0;
                                dummyspieler.anzahluber140 = 0;
                                dummyspieler.anzahluber100 = 0;
                                dummyspieler.anzahluber60 = 0;
                                dummyspieler.besterWurf = 0;
                                dummyspieler.checkoutmax = 0;
                                dummyspieler.matcheswon = 0;
                                dummyspieler.matcheslost = 0;
                                MainActivity.allespieler.set(index, dummyspieler);
                                MainActivity.speichern(statistik.this,false);
                                //anzeige aktualisieren
                                ds1.setText(String.valueOf(formater.format(dummyspieler.durchschnitt)));
                                pf1.setText(Integer.toString(dummyspieler.geworfenePfeile));
                                spg1.setText(Integer.toString(dummyspieler.AnzahlSpiele));
                                siege1.setText(Integer.toString(dummyspieler.anzahlSiege));
                                spe1.setText(Integer.toString(dummyspieler.AnzahlEinzelspiele));
                                u1801.setText(Integer.toString(dummyspieler.anzahl180));
                                u1401.setText(Integer.toString(dummyspieler.anzahluber140));
                                u1001.setText(Integer.toString(dummyspieler.anzahluber100));
                                u601.setText(Integer.toString(dummyspieler.anzahluber60));
                                besthit1.setText(Integer.toString(dummyspieler.besterWurf));
                                check1.setText(Integer.toString(dummyspieler.checkoutmax));
                                setlegs1.setText(Integer.toString(dummyspieler.matcheswon));
                                setlegs1lost.setText(Integer.toString(dummyspieler.matcheslost));
                                quote1.setText("0 %");

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                alertDialog.show();


            }
        });


        playerb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.willstStatistik) + spielerb.getSelectedItem().toString() + getResources().getString(R.string.wirklichl));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jal),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spielerdaten komplett löschen
                                int index = spielerb.getSelectedItemPosition();
                                MainActivity.spieler dummyspieler = MainActivity.allespieler.get(index);
                                dummyspieler.durchschnitt = 0;
                                dummyspieler.geworfenePfeile = 0;
                                dummyspieler.AnzahlSpiele = 0;
                                dummyspieler.anzahlSiege = 0;
                                dummyspieler.AnzahlEinzelspiele = 0;
                                dummyspieler.anzahl180 = 0;
                                dummyspieler.anzahluber140 = 0;
                                dummyspieler.anzahluber100 = 0;
                                dummyspieler.anzahluber60 = 0;
                                dummyspieler.besterWurf = 0;
                                dummyspieler.checkoutmax = 0;
                                dummyspieler.matcheswon = 0;
                                dummyspieler.matcheslost = 0;
                                MainActivity.allespieler.set(index, dummyspieler);
                                MainActivity.speichern(statistik.this,false);
                                //anzeige aktualisieren
                                ds2.setText(String.valueOf(formater.format(dummyspieler.durchschnitt)));
                                pf2.setText(Integer.toString(dummyspieler.geworfenePfeile));
                                spg2.setText(Integer.toString(dummyspieler.AnzahlSpiele));
                                siege2.setText(Integer.toString(dummyspieler.anzahlSiege));
                                spe2.setText(Integer.toString(dummyspieler.AnzahlEinzelspiele));
                                u1802.setText(Integer.toString(dummyspieler.anzahl180));
                                u1402.setText(Integer.toString(dummyspieler.anzahluber140));
                                u1002.setText(Integer.toString(dummyspieler.anzahluber100));
                                u602.setText(Integer.toString(dummyspieler.anzahluber60));
                                besthit2.setText(Integer.toString(dummyspieler.besterWurf));
                                check2.setText(Integer.toString(dummyspieler.checkoutmax));
                                setlegs2.setText(Integer.toString(dummyspieler.matcheswon));
                                setlegs2lost.setText(Integer.toString(dummyspieler.matcheslost));
                                quote2.setText("0 %");

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                alertDialog.show();
            }
        });

        alle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(statistik.this).create();
                alertDialog.setTitle(getResources().getString(R.string.achtung));
                alertDialog.setMessage(getResources().getString(R.string.allel));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jal),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //spielerdaten komplett löschen
                                MainActivity.spieler dummyspieler;
                                for (int index = 0; index < MainActivity.allespieler.size(); index++) {
                                    dummyspieler = MainActivity.allespieler.get(index);
                                    dummyspieler.durchschnitt = 0;
                                    dummyspieler.geworfenePfeile = 0;
                                    dummyspieler.AnzahlSpiele = 0;
                                    dummyspieler.anzahlSiege = 0;
                                    dummyspieler.AnzahlEinzelspiele = 0;
                                    dummyspieler.anzahl180 = 0;
                                    dummyspieler.anzahluber140 = 0;
                                    dummyspieler.anzahluber100 = 0;
                                    dummyspieler.anzahluber60 = 0;
                                    dummyspieler.besterWurf = 0;
                                    dummyspieler.checkoutmax = 0;
                                    dummyspieler.matcheswon = 0;
                                    dummyspieler.matcheslost = 0;
                                    MainActivity.allespieler.set(index, dummyspieler);
                                }

                                MainActivity.speichern(statistik.this,false);
                                //anzeige aktualisieren
                                ds1.setText("0");
                                pf1.setText("0");
                                spg1.setText("0");
                                siege1.setText("0");
                                spe1.setText("0");
                                u1801.setText("0");
                                u1401.setText("0");
                                u1001.setText("0");
                                u601.setText("0");
                                besthit1.setText("0");
                                check1.setText("0");
                                setlegs1.setText("0");
                                setlegs1lost.setText("0");
                                quote1.setText("0 %");

                                ds2.setText("0");
                                pf2.setText("0");
                                spg2.setText("0");
                                siege2.setText("0");
                                spe2.setText("0");
                                u1802.setText("0");
                                u1402.setText("0");
                                u1002.setText("0");
                                u602.setText("0");
                                besthit2.setText("0");
                                check2.setText("0");
                                setlegs2.setText("0");
                                setlegs2lost.setText("0");
                                quote2.setText("0 %");
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.abbrechen), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();


            }
        });


        spielera.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                MainActivity.spieler dummyspieler = MainActivity.allespieler.get(position);
                ds1.setText(String.valueOf(formater.format(dummyspieler.durchschnitt)));
                pf1.setText(Integer.toString(dummyspieler.geworfenePfeile));
                spg1.setText(Integer.toString(dummyspieler.AnzahlSpiele));
                siege1.setText(Integer.toString(dummyspieler.anzahlSiege));
                spe1.setText(Integer.toString(dummyspieler.AnzahlEinzelspiele));
                u1801.setText(Integer.toString(dummyspieler.anzahl180));
                u1401.setText(Integer.toString(dummyspieler.anzahluber140));
                u1001.setText(Integer.toString(dummyspieler.anzahluber100));
                u601.setText(Integer.toString(dummyspieler.anzahluber60));
                besthit1.setText(Integer.toString(dummyspieler.besterWurf));
                check1.setText(Integer.toString(dummyspieler.checkoutmax));
                setlegs1.setText(Integer.toString(dummyspieler.matcheswon));
                setlegs1lost.setText(Integer.toString(dummyspieler.matcheslost));
                if (!(dummyspieler.AnzahlSpiele == 0) && !(dummyspieler.AnzahlSpiele <= dummyspieler.AnzahlEinzelspiele)) {
                    float q11 = dummyspieler.AnzahlSpiele - dummyspieler.AnzahlEinzelspiele;
                    q11 = q11 / 100;
                    q11 = dummyspieler.anzahlSiege / q11;
                    quote1.setText(String.valueOf(formater.format(q11)) + " %");
                } else quote1.setText("0 %");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        spielerb.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                MainActivity.spieler dummyspieler2 = MainActivity.allespieler.get(position);
                ds2.setText(String.valueOf(formater.format(dummyspieler2.durchschnitt)));
                pf2.setText(Integer.toString(dummyspieler2.geworfenePfeile));
                spg2.setText(Integer.toString(dummyspieler2.AnzahlSpiele));
                siege2.setText(Integer.toString(dummyspieler2.anzahlSiege));
                spe2.setText(Integer.toString(dummyspieler2.AnzahlEinzelspiele));
                u1802.setText(Integer.toString(dummyspieler2.anzahl180));
                u1402.setText(Integer.toString(dummyspieler2.anzahluber140));
                u1002.setText(Integer.toString(dummyspieler2.anzahluber100));
                u602.setText(Integer.toString(dummyspieler2.anzahluber60));
                besthit2.setText(Integer.toString(dummyspieler2.besterWurf));
                check2.setText(Integer.toString(dummyspieler2.checkoutmax));
                setlegs2.setText(Integer.toString(dummyspieler2.matcheswon));
                setlegs2lost.setText(Integer.toString(dummyspieler2.matcheslost));
                if (!(dummyspieler2.AnzahlSpiele == 0) && !(dummyspieler2.AnzahlSpiele <= dummyspieler2.AnzahlEinzelspiele)) {
                    float q2 = dummyspieler2.AnzahlSpiele - dummyspieler2.AnzahlEinzelspiele;
                    q2 = q2 / 100;
                    q2 = dummyspieler2.anzahlSiege / q2;
                    quote2.setText(String.valueOf(formater.format(q2)) + " %");
                } else quote2.setText("0 %");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }
}
