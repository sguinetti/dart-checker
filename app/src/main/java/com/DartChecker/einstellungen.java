package com.DartChecker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class einstellungen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        boolean darktheme = true,
                slmode = true,
                inputmode = false,
                suddendeathmode = false;
        if (settings.contains("Theme")) darktheme = settings.getBoolean("Theme", true);
        if (darktheme) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_einstellungen);

        final Switch setmode = findViewById(R.id.slmode);
        final Switch themes = findViewById(R.id.themeswitch);
        TextView umschaltzeit = findViewById(R.id.millisekunden);
        final TextView dartanzahl = findViewById(R.id.dartanzahl);
        final Switch inputmethode = findViewById(R.id.inputmethode);
        final Switch suddendeath = findViewById(R.id.suddendeath);
        int anzahldarts=0;

        if (settings.contains("changetime"))
            umschaltzeit.setText(Integer.toString(settings.getInt("changetime", 1500)));

        if (settings.contains("setlegmodus")) slmode = settings.getBoolean("setlegmodus", true);
        setmode.setChecked(slmode);
        if (slmode) {
            setmode.setText(getResources().getString(R.string.setlegmodusfirstto));
        } else {
            setmode.setText(getResources().getString(R.string.setlegmodusbestof));
        }

        themes.setChecked(darktheme);
        if (darktheme) {
            themes.setText(getResources().getString(R.string.design_dunkel));
        } else {
            themes.setText(getResources().getString(R.string.design_hell));
        }

        if (settings.contains("inputmethode")) inputmode = settings.getBoolean("inputmethode",false);
        inputmethode.setChecked(inputmode);
        if (inputmode) {
            inputmethode.setText(getResources().getString(R.string.inputmode3dart));
        } else {
            inputmethode.setText(getResources().getString(R.string.inputmode1dart));
        }

        if (settings.contains("suddendeath")) suddendeathmode = settings.getBoolean("suddendeath",false);
        suddendeath.setChecked(suddendeathmode);
        if (settings.contains("sddarts")) anzahldarts =  settings.getInt("sddarts",20);
        dartanzahl.setText(Integer.toString(anzahldarts));
        if (suddendeath.isChecked()) dartanzahl.setEnabled(true);
        else dartanzahl.setEnabled(false);

        themes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    themes.setText(getResources().getString(R.string.design_dunkel));
                } else {
                    themes.setText(getResources().getString(R.string.design_hell));

                }
                Switch theme = findViewById(R.id.themeswitch);
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("Theme", theme.isChecked());
                editor.apply();
                //einstellungsactivity neu starten
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        findViewById(R.id.appBarLayout).requestFocus();


        setmode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setmode.setText(getResources().getString(R.string.setlegmodusfirstto));
                } else setmode.setText(getResources().getString(R.string.setlegmodusbestof));

                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("setlegmodus", setmode.isChecked());
                editor.apply();
            }
        });

        inputmethode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    inputmethode.setText(getResources().getString(R.string.inputmode3dart));
                } else inputmethode.setText(getResources().getString(R.string.inputmode1dart));

                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("inputmethode", inputmethode.isChecked());
                editor.apply();
            }
        });

        suddendeath.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dartanzahl.setEnabled(true);
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.suddendeathHinweis),Toast.LENGTH_LONG).show();
                }
                else dartanzahl.setEnabled(false);
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("suddendeath", suddendeath.isChecked());
                editor.apply();
            }
        });

        final Button ok = findViewById(R.id.okb2);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    public void onBackPressed() {
        //changetime-Einstellungen speichern
        TextView millisekunden = findViewById(R.id.millisekunden);
        TextView dartanzahl = findViewById(R.id.dartanzahl);
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("changetime", Integer.valueOf(millisekunden.getText().toString()));
        editor.putInt("sddarts", Integer.valueOf(dartanzahl.getText().toString()));
        editor.apply();
        finish();
    }
}
