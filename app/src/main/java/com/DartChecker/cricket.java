package com.DartChecker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class cricket extends AppCompatActivity {

    private long startTime = 0;
    private int bcolor;
    private int bcolorn;
    private int textcoloraktiv,
            textcolorpassiv;
    private float textsizeaktiv,
            textsizepassiv;
    private boolean t = false, d = false;
    private final View.OnClickListener doubletriple = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button doubleb = findViewById(R.id.doublebutton);
            Button tripleb = findViewById(R.id.triple);
            doubleb.setBackgroundColor(bcolorn);        // workaround for low api version (15)
            tripleb.setBackgroundColor(bcolorn);        // workaround for low api version (15);
            if (v.getId() == R.id.doublebutton) {
                if (t) {
                    t = false;
                    tripleb.setBackgroundColor(bcolorn);
                }
                if (!d) {
                    d = true;
                    doubleb.setBackgroundColor(bcolor);
                } else {
                    d = false;
                    doubleb.setBackgroundColor(bcolorn);
                }
            } else {
                if (d) {
                    d = false;
                    doubleb.setBackgroundColor(bcolorn);
                }
                if (!t) {
                    t = true;
                    tripleb.setBackgroundColor(bcolor);
                } else {
                    t = false;
                    tripleb.setBackgroundColor(bcolorn);
                }
            }
        }
    };
    private int spieleranzahl,
            aktiverSpieler = 1,       //index für namensarray
            xdart = 0,                //zähler für geworfene darts pro runde
            aufnahme,
            changetime = 1500,
            ii = -1;                   //index für undo-speicher
    private String s = "";
    private CharSequence spielmodus;
    private CharSequence spielvariante;
    private player[] spieler = new player[5];
    private ArrayList<pfeil> wuerfe = new ArrayList<pfeil>();
    private final View.OnClickListener undoclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Integer sender = v.getId();
            int dart, faktor, theoretischewurfpunkte, addpunkte, anzahltrefferabzug;
            TextView darts = findViewById(R.id.darts);

            if (ii >= 0) {
                if (xdart == 0) {
                    //spielerwechsel
                    xdart = 2;
                    s = "..";
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                    if (aktiverSpieler > 1) aktiverSpieler--;
                    else aktiverSpieler = spieleranzahl;
                    textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                    textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                    textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                } else xdart--;
                if (xdart == 1) s = ".";
                else if (xdart == 0) s = "";
                darts.setText(s);
                dart = wuerfe.get(ii).zahl;
                faktor = wuerfe.get(ii).faktor;
                addpunkte = wuerfe.get(ii).addpunkte;
                theoretischewurfpunkte = dart * faktor;
                //addpunkte sind 1x,2x oder 3x dart -> entsprechend treffer abziehn und score
                if (spielvariante.equals("Classic")) {
                    spieler[aktiverSpieler].score -= addpunkte;
                } else
                    if (spielvariante.equals("Cut Throat")) {
                        for (int i=1;i<=spieleranzahl;i++){
                            if ((i!=aktiverSpieler) && !(zahlClosed(i,dart)))
                                spieler[i].score -= addpunkte;
                        }
                    }

                if ((faktor > 0) && (dart > 0))
                    anzahltrefferabzug = (theoretischewurfpunkte - addpunkte) / dart;
                else anzahltrefferabzug = 0;
                if (closed(dart) && (anzahltrefferabzug > 0)) {
                    for (int i = 1; i <= spieleranzahl; i++) {
                        textfeld(dart, i, 1).setTextColor(textcolorpassiv);
                    }
                    switch (dart) {
                        case 10:
                            Button b10 = findViewById(R.id.b10);
                            b10.setEnabled(true);
                            break;
                        case 11:
                            Button b11 = findViewById(R.id.b11);
                            b11.setEnabled(true);
                            break;
                        case 12:
                            Button b12 = findViewById(R.id.b12);
                            b12.setEnabled(true);
                            break;
                        case 13:
                            Button b13 = findViewById(R.id.b13);
                            b13.setEnabled(true);
                            break;
                        case 14:
                            Button b14 = findViewById(R.id.b14);
                            b14.setEnabled(true);
                            break;
                        case 15:
                            Button b15 = findViewById(R.id.b15);
                            b15.setEnabled(true);
                            break;
                        case 16:
                            Button b16 = findViewById(R.id.b16);
                            b16.setEnabled(true);
                            break;
                        case 17:
                            Button b17 = findViewById(R.id.b17);
                            b17.setEnabled(true);
                            break;
                        case 18:
                            Button b18 = findViewById(R.id.b18);
                            b18.setEnabled(true);
                            break;
                        case 19:
                            Button b19 = findViewById(R.id.b19);
                            b19.setEnabled(true);
                            break;
                        case 20:
                            Button b20 = findViewById(R.id.b20);
                            b20.setEnabled(true);
                            break;
                        case 25:
                            Button b25 = findViewById(R.id.bull);
                            b25.setEnabled(true);
                            break;
                    }
                }
                spieler[aktiverSpieler].treffer[dart] -= anzahltrefferabzug;
                if (dart != 0)
                    switch (spieler[aktiverSpieler].treffer[dart]) {
                        case 0:
                            textfeld(dart, aktiverSpieler, 1).setText("");
                            break;
                        case 1:
                            textfeld(dart, aktiverSpieler, 1).setText("/");
                            break;
                        case 2:
                            textfeld(dart, aktiverSpieler, 1).setText("X");
                            break;
                        case 3:
                            textfeld(dart, aktiverSpieler, 1).setText("O");
                            break;
                    }
                String str = "";
                switch (faktor) {
                    case 0:
                        str = "undo: " + getResources().getString(R.string.daneben);
                        break;
                    case 1:
                        str = "undo: " + Integer.toString(dart);
                        break;
                    case 2:
                        str = "undo: Double " + Integer.toString(dart);
                        break;
                    case 3:
                        str = "undo: Triple " + Integer.toString(dart);
                        break;
                }


                Toast.makeText(cricket.this, str, Toast.LENGTH_SHORT).show();


                //score textfeld aktualisiere
                if (spielvariante.equals("Classic")) {
                    textfeld(1, aktiverSpieler, 2).setText(Integer.toString(spieler[aktiverSpieler].score));
                } else
                    if (spielvariante.equals("Cut Throat")) {
                        for (int i=1;i<=spieleranzahl;i++){
                            if (i!=aktiverSpieler)
                                textfeld(1, i, 2).setText(Integer.toString(spieler[i].score));
                        }
                    }


                ii--;
            } else Toast.makeText(cricket.this, R.string.keinundo, Toast.LENGTH_LONG).show();

        }
    };

    private boolean zahlClosed(int spielerindex, int dart) {

            if (spieler[spielerindex].treffer[dart] != 3) return false;
            else return true;
    }

    private final View.OnClickListener buttonclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Integer sender = v.getId();
            Button eingabe = findViewById(sender);
            int dart,
                    addpunkte = 0;

            //ergebnis = Integer.parseInt(score.getText().toString());

            // welche taste wurde gedrückt?
            switch (sender) {
                case R.id.bull:
                    dart = 25;
                    break;
                case R.id.enter:
                    dart = 0;
                    break;
                default:
                    try {
                        dart = Integer.parseInt(eingabe.getText().toString());
                    } catch (Exception e) {
                        dart = 255;         //fehlerwert - sollte niemals eintreten
                    }
                    break;
            }

            int faktor;
            if (d) //double?
            {
                faktor = 2;
                d = false;
                Button doubleb = findViewById(R.id.doublebutton);
                doubleb.setBackgroundColor(bcolorn);
            } else if (t)  //triple?
            {
                faktor = 3;
                t = false;
                Button tripleb = findViewById(R.id.triple);
                tripleb.setBackgroundColor(bcolorn);
                if (dart == 25) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.triplewirklich), Toast.LENGTH_LONG).show();
                    return;
                }
            } else //single!
            {
                faktor = 1;
            }


            pfeil aktuellerWurf;
            if (dart != 0) {
                int i;
                //folgende 3 zeilen zum undo-speichern
                aktuellerWurf = new pfeil();
                if (!closed(dart)) aktuellerWurf.faktor = faktor;
                else aktuellerWurf.faktor = 0;

                for (i = faktor; i > 0; i--) {
                    if (spieler[aktiverSpieler].treffer[dart] < 3) {
                        spieler[aktiverSpieler].treffer[dart]++;
                    } else addpunkte += dart;
                }

                if (!closed(dart)) {
                    aufnahme += addpunkte;
                    // klassisches Cricket?
                    if (spielvariante.equals("Classic")) {
                        spieler[aktiverSpieler].score += addpunkte;
                    }
                    // Cut Throat Cricket?
                        else if (spielvariante.equals("Cut Throat")) {
                                for (i=1;i<=spieleranzahl;i++){
                                    if ((i!=aktiverSpieler) && !(zahlClosed(i,dart)))
                                        spieler[i].score+=addpunkte;
                                }

                        }

                } else {
                    for (i = 1; i <= spieleranzahl; i++) {
                        textfeld(dart, i, 1).setTextColor(Color.RED);
                    }
                    switch (dart) {
                        case 10:
                            Button b10 = findViewById(R.id.b10);
                            b10.setEnabled(false);
                            break;
                        case 11:
                            Button b11 = findViewById(R.id.b11);
                            b11.setEnabled(false);
                            break;
                        case 12:
                            Button b12 = findViewById(R.id.b12);
                            b12.setEnabled(false);
                            break;
                        case 13:
                            Button b13 = findViewById(R.id.b13);
                            b13.setEnabled(false);
                            break;
                        case 14:
                            Button b14 = findViewById(R.id.b14);
                            b14.setEnabled(false);
                            break;
                        case 15:
                            Button b15 = findViewById(R.id.b15);
                            b15.setEnabled(false);
                            break;
                        case 16:
                            Button b16 = findViewById(R.id.b16);
                            b16.setEnabled(false);
                            break;
                        case 17:
                            Button b17 = findViewById(R.id.b17);
                            b17.setEnabled(false);
                            break;
                        case 18:
                            Button b18 = findViewById(R.id.b18);
                            b18.setEnabled(false);
                            break;
                        case 19:
                            Button b19 = findViewById(R.id.b19);
                            b19.setEnabled(false);
                            break;
                        case 20:
                            Button b20 = findViewById(R.id.b20);
                            b20.setEnabled(false);
                            break;
                        case 25:
                            Button b25 = findViewById(R.id.bull);
                            b25.setEnabled(false);
                            break;
                    }

                    Toast.makeText(cricket.this, dart + " " + getResources().getString(R.string.closed), Toast.LENGTH_SHORT).show();
                }


                //restlichen undo daten speichern
                ii++;
                if (!closed(dart)) {
                    //aktuellerWurf.faktor=faktor;
                    aktuellerWurf.addpunkte = addpunkte;
                } else {
                    aktuellerWurf.addpunkte = 0;
                    if (addpunkte > 0)
                        aktuellerWurf.faktor = faktor - (addpunkte / dart); //falls closed durch faktor
                }

                aktuellerWurf.zahl = dart;
                wuerfe.add(ii, aktuellerWurf);


                switch (spieler[aktiverSpieler].treffer[dart]) {
                    case 1:
                        textfeld(dart, aktiverSpieler, 1).setText("/");
                        break;
                    case 2:
                        textfeld(dart, aktiverSpieler, 1).setText("X");
                        break;
                    case 3:
                        textfeld(dart, aktiverSpieler, 1).setText("O");
                        break;
                }

                if (spielvariante.equals("Classic")) {
                    textfeld(1, aktiverSpieler, 2).setText(Integer.toString(spieler[aktiverSpieler].score));
                } else if (spielvariante.equals("Cut Throat")) {
                    for (i=1;i<=spieleranzahl;i++){
                        if (i!=aktiverSpieler)
                            textfeld(1, i, 2).setText(Integer.toString(spieler[i].score));
                    }
                }

                //spiel zuende?
                if (gewinner()) {
                    //spielende einleiten und aufrufen

                    long spielzeit = (System.currentTimeMillis() - startTime) / 1000;
                    Intent intent = new Intent(cricket.this, spielende.class);
                    intent.putExtra("anzahl", spieleranzahl);
                    intent.putExtra("cricket", true);
                    intent.putExtra("spielzeit", spielzeit);

                    intent.putExtra("erster", spieler[aktiverSpieler].spielerName);
                    intent.putExtra("ersterscore", spieler[aktiverSpieler].score);

                    if (spieleranzahl == 2) {
                        switch (aktiverSpieler) {
                            case 1:
                                intent.putExtra("zweiter", spieler[2].spielerName);
                                intent.putExtra("zweiterscore", spieler[2].score);
                                break;
                            case 2:
                                intent.putExtra("zweiter", spieler[1].spielerName);
                                intent.putExtra("zweiterscore", spieler[1].score);
                                break;
                        }
                    } else if (spieleranzahl == 3) {
                        switch (aktiverSpieler) {
                            case 1:
                                intent.putExtra("zweiter", spieler[2].spielerName);
                                intent.putExtra("zweiterscore", spieler[2].score);
                                intent.putExtra("dritter", spieler[3].spielerName);
                                intent.putExtra("dritterscore", spieler[3].score);
                                break;
                            case 2:
                                intent.putExtra("zweiter", spieler[3].spielerName);
                                intent.putExtra("zweiterscore", spieler[3].score);
                                intent.putExtra("dritter", spieler[1].spielerName);
                                intent.putExtra("dritterscore", spieler[1].score);
                                break;
                            case 3:
                                intent.putExtra("zweiter", spieler[1].spielerName);
                                intent.putExtra("zweiterscore", spieler[1].score);
                                intent.putExtra("dritter", spieler[2].spielerName);
                                intent.putExtra("dritterscore", spieler[2].score);
                                break;
                        }
                    } else if (spieleranzahl == 4) {
                        switch (aktiverSpieler) {
                            case 1:
                                intent.putExtra("zweiter", spieler[2].spielerName);
                                intent.putExtra("zweiterscore", spieler[2].score);
                                intent.putExtra("dritter", spieler[3].spielerName);
                                intent.putExtra("dritterscore", spieler[3].score);
                                intent.putExtra("vierter", spieler[4].spielerName);
                                intent.putExtra("vierterscore", spieler[4].score);
                                break;
                            case 2:
                                intent.putExtra("zweiter", spieler[1].spielerName);
                                intent.putExtra("zweiterscore", spieler[1].score);
                                intent.putExtra("dritter", spieler[3].spielerName);
                                intent.putExtra("dritterscore", spieler[3].score);
                                intent.putExtra("vierter", spieler[4].spielerName);
                                intent.putExtra("vierterscore", spieler[4].score);
                                break;
                            case 3:
                                intent.putExtra("zweiter", spieler[1].spielerName);
                                intent.putExtra("zweiterscore", spieler[1].score);
                                intent.putExtra("dritter", spieler[2].spielerName);
                                intent.putExtra("dritterscore", spieler[2].score);
                                intent.putExtra("vierter", spieler[4].spielerName);
                                intent.putExtra("vierterscore", spieler[4].score);
                                break;
                            case 4:
                                intent.putExtra("zweiter", spieler[1].spielerName);
                                intent.putExtra("zweiterscore", spieler[1].score);
                                intent.putExtra("dritter", spieler[2].spielerName);
                                intent.putExtra("dritterscore", spieler[2].score);
                                intent.putExtra("vierter", spieler[3].spielerName);
                                intent.putExtra("vierterscore", spieler[3].score);
                                break;
                        }
                    }
                    startActivity(intent);
                    finish();
                }
            } else {
                ii++;
                aktuellerWurf = new pfeil();
                aktuellerWurf.faktor = 0;
                aktuellerWurf.addpunkte = 0;
                aktuellerWurf.zahl = dart;
                wuerfe.add(ii, aktuellerWurf);
            }

            TextView darts = findViewById(R.id.darts);
            if (xdart < 2) {
                xdart++;
                s = s + (".");

            } else {  //Spielerwechsel
                xdart = 0;
                s = "";

                if (aufnahme > 0) {
                    buttonfreeze(true);
                    final TextView aufnahmetv = findViewById(R.id.aufnahmetv);
                    aufnahmetv.setText(Integer.toString(aufnahme));
                    aufnahmetv.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            buttonfreeze(false);
                            aufnahmetv.setVisibility(View.INVISIBLE);
                        }
                    }, changetime);
                    aufnahme = 0;
                }

                textfeld(1, aktiverSpieler, 3).setTextColor(textcolorpassiv);
                textfeld(1, aktiverSpieler, 2).setTextColor(textcolorpassiv);
                textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizepassiv);
                if (aktiverSpieler < spieleranzahl) aktiverSpieler++;
                else aktiverSpieler = 1;
                textfeld(1, aktiverSpieler, 3).setTextColor(textcoloraktiv);
                textfeld(1, aktiverSpieler, 2).setTextColor(textcoloraktiv);
                textfeld(1, aktiverSpieler, 3).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);
                textfeld(1, aktiverSpieler, 2).setTextSize(TypedValue.COMPLEX_UNIT_PX, textsizeaktiv);

            }
            darts.setText(s);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_cricket);

        TypedValue outValue = new TypedValue();
        cricket.this.getTheme().resolveAttribute(R.attr.colorButtonNormal, outValue, true);
        bcolor = outValue.data;
        cricket.this.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
        bcolorn = outValue.data;

        TextView p1name = findViewById(R.id.p1name);
        TextView p2name = findViewById(R.id.p2name);
        textcoloraktiv = p1name.getCurrentTextColor();
        textcolorpassiv = p2name.getCurrentTextColor();
        textsizeaktiv = p1name.getTextSize();
        textsizepassiv = p2name.getTextSize();
        Intent intent = getIntent();
        spieleranzahl = Integer.parseInt(intent.getStringExtra("spieleranzahl"));
        spielmodus = intent.getCharSequenceExtra("spielmodus");
        CharSequence spieler1n = intent.getCharSequenceExtra("spieler1");
        CharSequence spieler2n = intent.getCharSequenceExtra("spieler2");
        CharSequence spieler3n = intent.getCharSequenceExtra("spieler3");
        CharSequence spieler4n = intent.getCharSequenceExtra("spieler4");
        spielvariante = intent.getCharSequenceExtra("spielvariante");

        player s1 = new player();
        player s2 = new player();
        player s3 = new player();
        player s4 = new player();

        spieler[1] = s1;
        spieler[2] = s2;
        spieler[3] = s3;
        spieler[4] = s4;
        for (int a = 1; a <= spieleranzahl; a++) spieler[a].score = 0;

        spieler[1].spielerName = spieler1n.toString();
        spieler[2].spielerName = spieler2n.toString();
        spieler[3].spielerName = spieler3n.toString();
        spieler[4].spielerName = spieler4n.toString();

        TextView p3name = findViewById(R.id.p3name);
        TextView p4name = findViewById(R.id.p4name);
        TextView p2score = findViewById(R.id.p2score);
        TextView p3score = findViewById(R.id.p3score);
        TextView p4score = findViewById(R.id.p4score);
        // Namenszuweisung und Layout in Abhängigkeit von der Anzahl der Spieler ändern
        p1name.setText(spieler[1].spielerName);
        if (spieleranzahl == 2) {
            ConstraintLayout cl = findViewById(R.id.match);
            cl.getViewById(R.id.p2name).setRight(R.id.parent);
            ConstraintSet cs = new ConstraintSet();
            cs.clone(cl);
            cs.connect(p2name.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
            cs.applyTo(cl);
            p3name.setVisibility(View.GONE);
            p4name.setVisibility(View.GONE);
            p3score.setVisibility(View.GONE);
            p4score.setVisibility(View.GONE);
            p2name.setText(spieler[2].spielerName);
        } else if (spieleranzahl == 1) {
            p2name.setVisibility(View.GONE);
            p3name.setVisibility(View.GONE);
            p4name.setVisibility(View.GONE);
            p2score.setVisibility(View.GONE);
            p3score.setVisibility(View.GONE);
            p4score.setVisibility(View.GONE);
        } else if (spieleranzahl == 3) {
            p4name.setVisibility(View.GONE);
            p4score.setVisibility(View.GONE);
            p2name.setText(spieler[2].spielerName);
            p3name.setText(spieler[3].spielerName);
        } else {
            p1name.setText(spieler[1].spielerName);
            p2name.setText(spieler[2].spielerName);
            p3name.setText(spieler[3].spielerName);
            p4name.setText(spieler[4].spielerName);
        }

        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.enter);
        Button bundo = findViewById(R.id.undo);
        b10.setOnClickListener(buttonclick);
        b11.setOnClickListener(buttonclick);
        b12.setOnClickListener(buttonclick);
        b13.setOnClickListener(buttonclick);
        b14.setOnClickListener(buttonclick);
        b15.setOnClickListener(buttonclick);
        b16.setOnClickListener(buttonclick);
        b17.setOnClickListener(buttonclick);
        b18.setOnClickListener(buttonclick);
        b19.setOnClickListener(buttonclick);
        b20.setOnClickListener(buttonclick);
        daneben.setOnClickListener(buttonclick);
        bull.setOnClickListener(buttonclick);
        bdouble.setOnClickListener(doubletriple);
        btripel.setOnClickListener(doubletriple);
        bundo.setOnClickListener(undoclick);

        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        if (settings.contains("changetime")) changetime = settings.getInt("changetime", 1500);

        //Layout in Abhängikeit vom spielmodus ändern


        if (spielmodus.equals("15-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b13.setVisibility(View.GONE);
            b14.setVisibility(View.GONE);
        } else if (spielmodus.equals("14-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
            b13.setVisibility(View.GONE);
        } else if (spielmodus.equals("13-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
            b12.setVisibility(View.GONE);
        } else if (spielmodus.equals("12-Bull")) {
            b10.setVisibility(View.GONE);
            b11.setVisibility(View.GONE);
        } else if (spielmodus.equals("11-Bull")) {
            b10.setVisibility(View.GONE);
        }
        findViewById(android.R.id.content).invalidate();

        startTime=System.currentTimeMillis();
    }

    private TextView textfeld(int dart, int aktiverSpieler, int typ)
    // typ=1 -> trefferfeld
    // typ=2 -> scorefeld
    // typ=3 -> namensfeld
    {
        String stt = "";
        switch (typ) {
            case 1:
                if (dart == 25) {
                    stt = "tbull" + "p" + aktiverSpieler;
                } else {
                    stt = "t" + dart + "p" + aktiverSpieler;
                }
                break;
            case 2:
                stt = "p" + aktiverSpieler + "score";
                break;
            case 3:
                stt = "p" + aktiverSpieler + "name";
                break;
        }
        int idd = getResources().getIdentifier(stt, "id", getPackageName());
        return findViewById(idd);
    }

    private boolean closed(int dart) {
        int j;
        for (j = 1; j <= spieleranzahl; j++) {
            if (spieler[j].treffer[dart] != 3) return false;
        }
        return true;
    }

    private boolean gewinner() {
        if (spielvariante.equals("Classic")){
            // gewinner ist, der a) mehr punkte hat als alle anderen mitspieler
            for (int i = 1; i <= spieleranzahl; i++) {
                if ((spieler[aktiverSpieler].score < spieler[i].score) && (aktiverSpieler != i))
                    return false;
            }
        } else
            if (spielvariante.equals("Cut Throat")) {
            // gewinner ist, der a) am wenigsten Punkte hat als alle anderen mitspieler
                for (int i = 1; i <= spieleranzahl; i++) {
                    if ((spieler[aktiverSpieler].score > spieler[i].score) && (aktiverSpieler != i))
                        return false;
                }

            }

        // und b) der sämtliche zahlen (x-20) geschlossen  (d.h. 3x getroffen) hat
        for (int d = Integer.parseInt(spielmodus.subSequence(0, 2).toString()); d <= 20; d++) {
            if (spieler[aktiverSpieler].treffer[d] != 3) return false;
        }
        // und bull
        return spieler[aktiverSpieler].treffer[25] == 3;
    }

    private void buttonfreeze(boolean freeze) {
        Button b10 = findViewById(R.id.b10);
        Button b11 = findViewById(R.id.b11);
        Button b12 = findViewById(R.id.b12);
        Button b13 = findViewById(R.id.b13);
        Button b14 = findViewById(R.id.b14);
        Button b15 = findViewById(R.id.b15);
        Button b16 = findViewById(R.id.b16);
        Button b17 = findViewById(R.id.b17);
        Button b18 = findViewById(R.id.b18);
        Button b19 = findViewById(R.id.b19);
        Button b20 = findViewById(R.id.b20);
        Button bull = findViewById(R.id.bull);
        Button bdouble = findViewById(R.id.doublebutton);
        Button btripel = findViewById(R.id.triple);
        Button daneben = findViewById(R.id.enter);
        Button bundo = findViewById(R.id.undo);

        freeze = !freeze;       //eine frage der logik ;-)

        bdouble.setEnabled(freeze);
        btripel.setEnabled(freeze);
        daneben.setEnabled(freeze);
        bundo.setEnabled(freeze);
        if (!freeze) {
            b10.setEnabled(freeze);
            b11.setEnabled(freeze);
            b12.setEnabled(freeze);
            b13.setEnabled(freeze);
            b14.setEnabled(freeze);
            b15.setEnabled(freeze);
            b16.setEnabled(freeze);
            b17.setEnabled(freeze);
            b18.setEnabled(freeze);
            b19.setEnabled(freeze);
            b20.setEnabled(freeze);
            bull.setEnabled(freeze);

        } else {
            if (!closed(10)) b10.setEnabled(freeze);
            if (!closed(11)) b11.setEnabled(freeze);
            if (!closed(12)) b12.setEnabled(freeze);
            if (!closed(13)) b13.setEnabled(freeze);
            if (!closed(14)) b14.setEnabled(freeze);
            if (!closed(15)) b15.setEnabled(freeze);
            if (!closed(16)) b16.setEnabled(freeze);
            if (!closed(17)) b17.setEnabled(freeze);
            if (!closed(18)) b18.setEnabled(freeze);
            if (!closed(19)) b19.setEnabled(freeze);
            if (!closed(20)) b20.setEnabled(freeze);
            if (!closed(25)) bull.setEnabled(freeze);
        }

    }

    public void onBackPressed() {

        AlertDialog alertDialog = new AlertDialog.Builder(cricket.this).create();
        alertDialog.setTitle(getResources().getString(R.string.achtung));
        alertDialog.setMessage(getResources().getString(R.string.willstduverlassen));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.jaichw),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.zuruck), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private static class player {
        String spielerName;
        int treffer[] = new int[26]; // feld 25 ist anzahl_bull_treffer, ansonsten 10-20;
        int score;
        int darts;
    }

    private class pfeil {
        int zahl;
        int faktor;
        int addpunkte;
    }


}

