Dart Checker
------------

An app for Android, that should assist players of dart in counting their score.

game type:  X01, SET/LEG and FREE mode for training, CRICKET

player:     1-4

includes game statistic, undo function, double and single out, checkout suggestions..

[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.DartChecker/)