hinzugefügt:
- Anzeige der Spieldauer am Ende eines Matches

repariert:
- Fehler im Match Modus: "Undo" bewirkte einen Spielerwechsel, wenn noch kein Dartwurf eingegeben wurde
- fehlende englische Übersetung im Cricket Modus
