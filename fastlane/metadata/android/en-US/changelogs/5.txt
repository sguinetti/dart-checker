- layout improvements in cricket mode

added:
- new preference item: FIRST TO or BEST OF
- set/leg status popup after leg finish

fixed:
- bug in display of "highest checkout" after SET/LEG match
- bug in cricket: (re)activation of buttons, while numbers being closed
