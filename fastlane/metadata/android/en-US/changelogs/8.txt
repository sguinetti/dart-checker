added:
- support of landscape screen orientation in match mode

fixed:
- sometimes wrong display of playing time
- screen standby in cricket mode
- layout of names in cricket mode